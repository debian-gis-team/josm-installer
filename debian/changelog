josm-installer (0.0.6+svn19237) unstable; urgency=medium

  * Updated linux directory to r19237.
  * Demote openjfx to Recommends, RC buggy.
  * Update lintian overrides.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 01 Nov 2024 05:49:35 +0100

josm-installer (0.0.6+svn19215) unstable; urgency=medium

  * Add openjfx to dependencies.
    (closes: #1081959)
  * Updated linux directory to r19215.
  * Incorporate MaxRAMPercentag changes in josm launcher.
  * Update lintian overrides.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 02 Oct 2024 15:34:05 +0200

josm-installer (0.0.5+svn19018) unstable; urgency=medium

  * Update version_url for tested version.
  * Use dh_installsystemd to install systemd service and timer.
    (closes: #1073694)
  * Bump Standards-Version to 4.7.0, no changes.

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 06 Aug 2024 15:25:15 +0200

josm-installer (0.0.4+svn19018) unstable; urgency=medium

  * Remove support for JRE <= 10 from josm launcher.
  * Updated linux directory to r19018.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 11 Apr 2024 05:30:11 +0200

josm-installer (0.0.3+svn18985) unstable; urgency=medium

  * Updated linux directory to r18985.
  * Require at least java11-runtime.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 18 Mar 2024 16:19:12 +0100

josm-installer (0.0.3+svn18889) unstable; urgency=medium

  * Bump debhelper compat to 13.
  * Use execute_after instead of override in rules file.
  * Enable Salsa CI.
  * Updated linux directory to r18889.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 06 Dec 2023 07:08:47 +0100

josm-installer (0.0.2+svn18699) unstable; urgency=medium

  * Move from experimental to unstable.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 11 Jun 2023 10:57:03 +0200

josm-installer (0.0.2+svn18699~exp1) experimental; urgency=medium

  * Add Rules-Requires-Root to control file.
  * Bump Standards-Version to 4.6.2, no changes.
  * Updated linux directory to r18699.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 03 Apr 2023 16:54:53 +0200

josm-installer (0.0.2+svn18515) unstable; urgency=medium

  * Fix flake8 'E275 missing whitespace after keyword' issues.
    (closes: #1021288)
  * Change alignment of closing parenthesis.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 05 Oct 2022 05:40:40 +0200

josm-installer (0.0.1+svn18515) unstable; urgency=medium

  * Updated linux directory to r18515.
  * Merge josm launcher improvements.

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 02 Aug 2022 06:41:21 +0200

josm-installer (0.0.1+svn18500) unstable; urgency=medium

  * Bump Standards-Version to 4.6.1, no changes.
  * Updated linux directory to r18500.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 07 Jul 2022 05:58:27 +0200

josm-installer (0.0.1+svn18402) unstable; urgency=medium

  * Updated linux directory to r18402.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 06 Apr 2022 05:40:31 +0200

josm-installer (0.0.1+svn18304) unstable; urgency=medium

  * Updated linux directory to r18304.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 03 Jan 2022 08:43:30 +0100

josm-installer (0.0.1+svn18297) unstable; urgency=medium

  * Bump Standards-Version to 4.6.0, no changes.
  * Bump debhelper compat to 12, changes:
    - Drop --list-missing from dh_install
  * Update lintian overrides.
  * Updated linux directory to r18297.
  * Move systemd files back to /lib.

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 02 Nov 2021 06:03:30 +0100

josm-installer (0.0.1+svn18171) unstable; urgency=medium

  * Updated linux directory to r18171.
  * Install systemd service and timer in /usr/lib instead of /usr.
  * Update lintian overrides.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 02 Sep 2021 06:12:46 +0200

josm-installer (0.0.1+svn17970) unstable; urgency=medium

  * Move from experimental to unstable.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 15 Aug 2021 14:26:36 +0200

josm-installer (0.0.1+svn17970~exp1) experimental; urgency=medium

  * Updated linux directory to r17970.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 12 Jul 2021 07:58:06 +0200

josm-installer (0.0.1+svn17641~exp1) experimental; urgency=medium

  * Updated linux directory to r17641.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 02 Apr 2021 03:51:45 +0200

josm-installer (0.0.1+svn17566~exp1) experimental; urgency=medium

  * Updated linux directory to r17566.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 19 Mar 2021 06:28:06 +0100

josm-installer (0.0.1+svn17530~exp1) experimental; urgency=medium

  * Updated linux directory to r17530.
  * Add fonts-noto to dependencies.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 15 Mar 2021 06:02:34 +0100

josm-installer (0.0.1+svn17345) unstable; urgency=medium

  * Update lintian overrides.
  * Bump Standards-Version to 4.5.1, no changes.
  * Updated linux directory to r17345.

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 29 Dec 2020 20:21:58 +0100

josm-installer (0.0.1+svn16524) unstable; urgency=medium

  * Updated linux directory to r16524.

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 02 Jun 2020 20:56:00 +0200

josm-installer (0.0.1+svn16006) unstable; urgency=medium

  * Handle ~ suffix in update-linux.sh.
  * Bump debhelper compat to 10, changes:
    - Drop --parallel option, enabled by default
    - Don't explicitly enable systemd, enabled by default
  * Add default file to pass options to the systemd service.
  * Move from experimental to unstable.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 08 Apr 2020 20:35:16 +0200

josm-installer (0.0.1+svn16006~exp1) experimental; urgency=medium

  * Initial release (Closes: #953722)

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 12 Mar 2020 15:06:24 +0100
